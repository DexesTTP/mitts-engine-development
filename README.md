# mitts-engine-development
Application for developing games for Mitts-Engine (contribute games [here](https://gitlab.com/strawberria/mitts-engine-games)).  
Built using [Wails v2](https://wails.io/) with a Svelte + TypeScript + TailwindCSS frontend. 

## Installation
- Download and install Go from their official [downloads page](https://go.dev/dl/)
- Download Wails following their [installation guide](https://wails.io/docs/gettingstarted/installation)
- From the project's top-level, run `wails dev` to initialize a development instance, or `wails build -debug` to compile (`-debug` flag for with inspect element, etc.)
- Upon building, retrieve the built binary from `build/bin` - note that antiviruses may falsely flag Wails-built binaries as malware.

## Changelog / Updating
Breaking changes are those across major versions (1.4.0 -> 1.5.0 for instance) which require the porting of project data (TODO). Changelog can be found [here](documentation/changelog.md) and document changes and whether said changes are breaking.