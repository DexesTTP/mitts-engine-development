## 1.4.2 (07/14/2022)
- Fixed bug `Cannot read properties of undefined (reading 'action')`
- Fixed bug `Cannot read properties of undefined (reading 'devname')` in the context of Svelte component generation